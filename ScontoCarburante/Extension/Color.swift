//
//  Color.swift
//  ScontoCarburante
//
//  Created by aleksandar.aleksic on 19.11.21..
//

import SwiftUI

extension Color{
    
    public static var greenMain: Color{
        return Color(uiColor: UIColor(named: "GreenMain")!)
    }
    
    public static var greenLight: Color{
        return Color(uiColor: UIColor(named: "GreenLight")!)
    }
    
    public static var firstToWhite: Color{
        return Color(red: 0.956, green: 0.959, blue: 0.966)
    }

    public static var darkBlue: Color{
        return Color(uiColor: UIColor(named: "DarkBlue")!)
    }
    
    public static var buttonBlue: Color{
        return Color(uiColor: UIColor(named: "ButtonBlue")!)
    }
    
    public static var textGray: Color{
        return Color(uiColor: UIColor(named: "TextGray")!)
    }
    
    public static var orangeSC: Color{
        return Color(uiColor: UIColor(named: "OrangeSC")!)
    }
}
