//
//  ScontoCarburanteApp.swift
//  ScontoCarburante
//
//  Created by lazar.velimirovic on 12/15/21.
//

import SwiftUI

@main
struct ScontoCarburanteApp: App {
    var body: some Scene {
        WindowGroup {
            HomeView()
        }
    }
}
