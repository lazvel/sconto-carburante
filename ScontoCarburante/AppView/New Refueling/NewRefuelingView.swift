//
//  NewRefuelingView.swift
//  ScontoCarburanteDemo
//
//  Created by lazar.velimirovic on 12/2/21.
//

import SwiftUI

struct NewRefuelingView: View {
    @State private var counter = 0
    @State private var shouldShow = false
    
    let transaction = FuelHistory.transactions.first
    
    var body: some View {
        NavigationView {
            ZStack {
                VStack {
                    VStack(spacing: 30) {
                        VStack {
                            Image("greenCheck")
                                .opacity(counter != 3 ? 0 : 1)
                            Text(counter != 3 ? "Nuovo rifornimento" : "Rifornimento\nandato a buon fine")
                                .font(.title.bold())
                                .fixedSize(horizontal: false, vertical: true)
                                .multilineTextAlignment(.center)
                        }
                        
                        Text(counter != 3 ? "Veicolo **CX879V**" : "Numero Transazione: **xxxxxxx**")
                        
                        // first view
                        if counter == 0 {
                            FirstRefuelingView(counter: $counter)
                        } else if counter == 1 {
                            SecondRefuelingView(counter: $counter)
                        } else if counter == 2 {
                            ExpandableRowView(transaction: transaction!)
                                .padding()
                                .background(Color.textGray)
                                .cornerRadius(5)
                        } else {
                            ExpandableRowView(transaction: transaction!)
                                .padding()
                                .background(Color.textGray)
                                .cornerRadius(5)
                        }
                        
                        // end of the first view
                        Spacer()
                        
                        // hidden button
                        VStack {
                            if counter == 3 {
                                Button {
                                    shouldShow.toggle()
                                } label: {
                                    Text(counter != 3 ? "Conferma" : "Torna alla home")
                                        .padding()
                                        .frame(width: UIScreen.main.bounds.width - 60)
                                        .font(.title2.bold())
                                        .foregroundColor(.white)
                                        .background(Color.greenMain)
                                        .cornerRadius(5)
                                        
                                }
                                .opacity(counter == 2 || counter == 3 ? 1 : 0)
                                .fullScreenCover(isPresented: $shouldShow) {
//                                    HPView()
                                }
                            } else {
                                Button {
                                    counter += 1
    //                                shouldShow.toggle()
                                } label: {
                                    Text(counter != 3 ? "Conferma" : "Torna alla home")
                                        .padding()
                                        .frame(width: UIScreen.main.bounds.width - 60)
                                        .font(.title2.bold())
                                        .foregroundColor(.white)
                                        .background(Color.greenMain)
                                        .cornerRadius(5)
                                        
                                }
                                .opacity(counter == 2 || counter == 3 ? 1 : 0)
                                }
                            
                            Button {
                            } label: {
                                Text("Annulla")
                                    .font(.title3.weight(.semibold))
                            }
                            .padding()
                        }
//                        .padding(.top)
                    }
                    .frame(maxWidth: .infinity, maxHeight: .infinity)
                    .background(.white)
                    
                }
                .foregroundColor(.darkBlue)
                .background(.gray)
                .padding(.top, 40)
            }
            .toolbar {
                ToolbarItemGroup(placement: .navigationBarLeading) {
                    Text("Sconto Carburante")
                        .font(.headline.weight(.semibold))
                        .foregroundColor(Color.darkBlue)
                        
                }
                
                ToolbarItemGroup(placement: .navigationBarTrailing) {
                    NavButtonsView()
                }
               
                ToolbarItem(placement: .navigationBarLeading) {
                    Rectangle()
                        .frame(width: UIScreen.main.bounds.width, height: 2)
                        .foregroundColor(Color.greenMain)
                        .offset(x: -30, y: 20)
                        
                }
            }
        }
       
    }
}

struct FirstRefuelingView: View {
    @Binding var counter: Int
    
    var body: some View {
        VStack(spacing: 30) {
            Image("qr")
                .resizable()
                .frame(width: 170, height: 170)
                .onTapGesture {
                    counter = 1
                }
            
            Text("Mostra il QR code al benzinaio oppure\ncomunica il codice sottostante")
                .multilineTextAlignment(.center)
                .fixedSize(horizontal: false, vertical: true)
            
            Text("204578")
                .font(.largeTitle.bold())
        }
    }
}

struct SecondRefuelingView: View {
    @Binding var counter: Int
    
    var body: some View {
        VStack(spacing: 30) {
            Spacer()
            
            ZStack {
                Image("orangeCircle")
                Image("insideOrange")
            }
            .onTapGesture {
                counter = 2
            }
            
            Text("Rifornimento in corso")
                .font(.title3.weight(.semibold))
                .multilineTextAlignment(.center)
                .foregroundColor(.orange)
            
            Spacer()
            
            Text("L'esercenta sta inserendo i dati.\nAttendi auqlche secondo, al termine potrai\n visualizzare i dati e confermare.")
                .multilineTextAlignment(.center)
        }
    }
}

struct NewRefuelingView_Previews: PreviewProvider {
    static var previews: some View {
        NewRefuelingView()
    }
}
