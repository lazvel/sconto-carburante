//
//  VehicleAlertView.swift
//  ScontoCarburante
//
//  Created by aleksandar.aleksic on 26.11.21..
//

import SwiftUI

struct VehicleAlertView: View {
    
    @Binding var alertIsShown: Bool
    @State var confirmAlert: Bool = false
    
    var body: some View {
        ZStack{
            AlertView(title: "Sei sicuro?",
                      description: "Stai rimuovendo il veicolo. Per inserirne uno nuovo visita il sito dedicato al servizio.",
                      alertType: .warrning,
                      alternativeView: {
                return AnyView(
                    VStack{
                        Button{
                            confirmAlert.toggle()
                        }label: {
                            Text("Elimina")
                                .font(.title3).bold()
                        }
                        .frame(width: 160, height: 50)
                        .background(.red)
                        .foregroundColor(.white)
                        .cornerRadius(5)
                        
                        Button{
                            alertIsShown.toggle()
                        }label: {
                            Text("Annulla")
                                .font(.title3).bold()
                        }
                        .frame(width: 160, height: 50)
                        .background(.white)
                        .foregroundColor(Color.greenMain)
                        .border(Color.greenMain, width: 1.5)
                        .cornerRadius(3)
                    }
                        .padding(.bottom, 50)
                )
            }(),
                      alertIsShown: $alertIsShown)
            
            if confirmAlert{
                AlertView(title: "Veicolo eliminato correttamente",
                          description: "", buttonTitle: "Ok grazie",
                          alertType: .success,
                          alertIsShown: $alertIsShown)
            }
            
        }
    }
}
