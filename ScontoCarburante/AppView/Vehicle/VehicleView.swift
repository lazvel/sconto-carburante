//
//  VeicoliView.swift
//  ScontoCarburante
//
//  Created by aleksandar.aleksic on 26.11.21..
//

import SwiftUI

struct VehicleView: View {
    
    @Environment (\.presentationMode) var presentationMode
    
    var vehicles: [Vehicle] = DemoData.vehicles
    
    @State var alertIsShown: Bool = false
    
    var body: some View {
            ZStack{
                VStack(alignment: .leading, spacing: 15){
                    Text("Veicoli")
                        .font(.title).bold()
                        .foregroundColor(.darkBlue)
                        .padding(.top, 15)
                    
                    Text("Questa e la lista dei veicoli registrati. Se uno di questi non e piu in tuo possesso puoi inviare la richiesta di rimozione cliccando su 'Elimina'")
                        .font(.system(size: 15, weight: .light))
                        .foregroundColor(.darkBlue)

                    ForEach(0...(vehicles.count - 1), id: \.self){ i in
                        let vehicle = vehicles[i]
                        HStack{
                            VStack(spacing: 0){
                                chooseImage(vehicleType: vehicle.type)
                                    .frame(width: 60, height: 50)
                                
                                Text("\(vehicle.name)")
                                    .foregroundColor(Color.greenMain)
                                    .font(.system(size: 16))
                                    .frame(maxWidth: .infinity)
                            }
                            .frame(width: 70)
                            .padding(.leading, 10)
                            
                            Spacer()
                            
                            Button{
                                alertIsShown.toggle()
                            }label: {
                                Image(systemName: "trash")
                                Text("Elimina").bold()
                            }
                            .frame(width: 110, height: 45)
                            .background(Color.greenMain)
                            .cornerRadius(5)
                            .foregroundColor(.white)
                            .padding(.trailing, 15)
                        }
                        .background(.white)
                        .padding(.leading, 15)
                        .padding(.trailing, 15)
                    }
                    Spacer()
                }
                .padding(15)
                
                NavigationBottomLine()
                
                Spacer()
                
                if alertIsShown{
                    VehicleAlertView(alertIsShown: $alertIsShown)
                }
            }
            .background(Color.firstToWhite)
            .toolbar {
                ToolbarItemGroup(){
                    ToolbarCarburante(hasBackButton: true, backButtonTitle: "Torna alla Home") {
                        self.presentationMode.wrappedValue.dismiss()
                    }
                }
            }
            .navigationBarBackButtonHidden(true)
    }
    
    private func chooseImage(vehicleType: VehicleType) -> Image{
        return Image("\(vehicleType)Green")
    }
}

struct VeicoliView_Previews: PreviewProvider {
    static var previews: some View {
        VehicleView()
    }
}
