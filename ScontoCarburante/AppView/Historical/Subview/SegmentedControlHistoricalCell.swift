//
//  SegmentedControlHistoricalCell.swift
//  ScontoCarburante
//
//  Created by aleksandar.aleksic on 29.11.21..
//

import SwiftUI

struct SegmentedControlHistoricalCell: View {
    
    @Binding var selectedVehicle: Int
    @State var serialNumber: Int
    var vehicleName: String
    
    var body: some View {
        VStack{
            Text("\(vehicleName)")
                .font(.body.bold())
                .foregroundColor(isCellSelected() ? .white : Color.textGray)
                .font(.system(size: 16))
                .frame(maxWidth: .infinity)
                .padding(5)
            
        }
        .frame(width: 80, height: 50)
        .background(isCellSelected() ? Color.greenMain : Color.firstToWhite)
        .border(Color.textGray, width: isCellSelected() ? 0 : 2)
        .cornerRadius(6)
        .onTapGesture {
            selectedVehicle = serialNumber
        }
    }
    
    private func isCellSelected() -> Bool{
        return selectedVehicle == serialNumber
    }
    
    private func getColor(serialNumber: Int) -> Color{
        if selectedVehicle == serialNumber{
            return Color.greenMain
        }
        return Color.darkBlue
    }
}

struct SegmentedControlHistoricalCell_Previews: PreviewProvider {
    static var previews: some View {
        SegmentedControlHistoricalCell(
            selectedVehicle: .constant(1),
            serialNumber: 0,
            vehicleName: DemoData.vehicles[0].name)
    }
}
