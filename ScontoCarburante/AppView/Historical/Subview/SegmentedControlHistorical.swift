//
//  SegmentedControlHistorical.swift
//  ScontoCarburante
//
//  Created by aleksandar.aleksic on 29.11.21..
//

import SwiftUI

struct SegmentedControlHistorical: View {
    var vehicles: [String]
    
    @State var selectedVehicle: Int = 0
    
    var body: some View {
        HStack(spacing: 7){
            
            ForEach(0...(vehicles.count - 1), id: \.self){
            SegmentedControlHistoricalCell(
                selectedVehicle: $selectedVehicle,
                serialNumber: $0,
                vehicleName: vehicles[$0])
            }
            
            Spacer()
        }
        .padding(EdgeInsets(top: 2,
                            leading: 25,
                            bottom: 2,
                            trailing: 25))
    }
}

struct SegmentedControlHistorical_Previews: PreviewProvider {
    static var previews: some View {
        SegmentedControlHistorical(vehicles: DemoData.vehicles.map{ $0.name})
    }
}
