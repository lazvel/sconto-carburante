//
//  TransactionRow.swift
//  ScontoCarburante
//
//  Created by aleksandar.aleksic on 29.11.21..
//

import SwiftUI

struct TransactionRow: View{
    var transaction: Transaction
    
    @State private var expandCell: Bool = false
    
    var body: some View{
        VStack{
            HStack(spacing: 30){
                HStack(alignment: .center){
                    Image("fuelAutomat")
                        .frame(width: 40, height: 40)
                        .clipShape(Circle())
                        .background(Color.firstToWhite)
                }
                .clipShape(Circle())
                .padding(.leading, 5)
                
                VStack(alignment: .leading, spacing: 10){
                    let transAmount = String(format: "%.2f", transaction.amount)
                    Text(transaction.date)
                    Text("Hai risparmiato ") + Text("\(transAmount)$").bold()
                }
                .padding(5)
                .foregroundColor(Color.darkBlue)
                
                Button{
                    expandCell.toggle()
                } label: {
                    Image("arrowUp")
                        .rotationEffect(
                            Angle(degrees: expandCell ? 0 : 180)
                        )
                        .padding(.bottom, expandCell ? 0 : 5)
                }
                .padding(.trailing, 5)
            }
            
            if expandCell{
                TransactionRowExtended(transaction: transaction)
            }
            
        }
        .padding(3)
        .background(.white)
        .cornerRadius(8)
    }
}

struct TransactionRow_Previews: PreviewProvider {
    static var previews: some View {
        TransactionRow(transaction: DemoData.transactions[0])
    }
}

struct TransactionRowExtended: View{
    var transaction: Transaction
    
    var body: some View{
        VStack(alignment: .leading, spacing: 18){
            VStack(alignment: .leading){
                Text("Codice punto vendita: ") + Text("\(transaction.saleCode)").bold()
                Text("Prezzo al litro: ") + Text("\(transaction.pricePerLiter, specifier: "%.3f")$").bold()
                Text("Litri erogati: ") + Text("\(transaction.dispensedLiters, specifier: "%.2f")L").bold()
                Text("Lordo: ") + Text("\(transaction.gross, specifier: "%.2f")$").bold()
            }
            
            VStack(alignment: .leading){
                Text("Sconto $/L: ") + Text("\(transaction.discountEurByLiter, specifier: "%.3f")").bold()
                Text("Prezzo al litro scontato: ") + Text("\(transaction.discountPerLiter, specifier: "%.3f")$").bold()
                Text("Litri scontati: ") + Text("\(transaction.discountedLiters, specifier: "%.2f")$").bold()
                Text("Sconto: ") + Text("\(transaction.discount, specifier: "%.2f")$").bold()
            }
            
            VStack(alignment: .leading){
                Text("Importo da pagare: ") + Text("\(transaction.amountToBePaid, specifier: "%.2f")$").bold()
                Text("Plafond residuo: ") + Text("\(transaction.residual)").bold()
                Text("Data del rifornimento: ") + Text("\(transaction.refuelingDate)").bold()
                Text("Orario del riforniment: ") + Text("\(transaction.refuelingTime)").bold()
            }
        }
        .padding(EdgeInsets(top: 12, leading: 5, bottom: 12, trailing: 5))
        .foregroundColor(Color.darkBlue)
    }
}
