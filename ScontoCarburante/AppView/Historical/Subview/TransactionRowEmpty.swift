//
//  TransactionRowEmpty.swift
//  ScontoCarburante
//
//  Created by aleksandar.aleksic on 30.11.21..
//

import SwiftUI

struct TransactionRowEmpty: View {
    var body: some View {
        ZStack{
            Capsule()
                .stroke(Color.darkBlue, style: StrokeStyle(lineWidth: 1.5, lineCap: .square,  dash: [CGFloat(15)]))
            Text("Non sono presenti \nrifornimenti")
                .multilineTextAlignment(.center)
                .font(.system(size: 19, weight: .regular))
        }
        .frame(height: 80)
        .foregroundColor(Color.darkBlue)
        .scaleEffect(0.8)
    }
}

struct TransactionRowEmpty_Previews: PreviewProvider {
    static var previews: some View {
        TransactionRowEmpty()
    }
}
