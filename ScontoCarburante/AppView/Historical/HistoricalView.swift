//
//  HistoricalView.swift
//  ScontoCarburante
//
//  Created by aleksandar.aleksic on 29.11.21..
//

import SwiftUI

struct HistoricalView: View {
    
    @Environment (\.presentationMode) var presentationMode
    
    @State private var selectedVehicle = ""
    
    var vehicles = DemoData.vehicles.map{ $0.name}
    var supplayList = DemoData.transactions

    var body: some View {
        ZStack{
            VStack{
                VStack(alignment: .leading){
                    Text("Storico")
                        .font(.system(size: 36, weight: .semibold))
                        .foregroundColor(Color.darkBlue)
                        .padding(.top, 25)
                        .padding(.leading, 50)
                    
                    SegmentedControlHistorical(vehicles: vehicles)
                        .scaleEffect(0.85)
                }
                
                VStack(alignment: .leading){


                    List{
                        Section(header:
                                    Label {
                                        Text("Settembre 2020")
                                            .foregroundColor(Color.darkBlue)
                                    } icon: {
                                        Image(systemName: "calendar")
                                            .foregroundColor(Color.darkBlue)
                                    }
                        ){
                            ForEach(supplayList){ transaction in
                                TransactionRow(transaction: transaction)
                            }
                        }
                        .listRowBackground(Color(UIColor.clear))
                        
                        Section(header:
                                    Label {
                                        Text("Augosto 2020")
                                            .foregroundColor(Color.darkBlue)
                                    } icon: {
                                        Image(systemName: "calendar")
                                            .foregroundColor(Color.darkBlue)
                                    }
                        ){
                            ForEach(supplayList){ transaction in
                                TransactionRow(transaction: transaction)
                            }
                        }
                        .listRowBackground(Color(UIColor.clear))
                        
                        Section(header: Label {
                            Text("Jul 2020")
                                .foregroundColor(Color.darkBlue)
                        } icon: {
                            Image(systemName: "calendar")
                                .foregroundColor(Color.darkBlue)
                        }){
                            TransactionRowEmpty()
                        }
                        .listRowBackground(Color(UIColor.clear))
                    }
                    .padding(.leading)
                    .listStyle(SidebarListStyle())
                    .onAppear{
                        UITableView.appearance().backgroundColor = .clear
                    }
                }
                
                Spacer()
            }
            
            NavigationBottomLine()
               
        }
        .background(Color.firstToWhite)
        .toolbar {
            ToolbarItemGroup(){
                ToolbarCarburante(hasBackButton: true, backButtonTitle: "Torna alla Home") {
                    self.presentationMode.wrappedValue.dismiss()
                }
            }
        }
        .navigationBarBackButtonHidden(true)
    }
}

struct HistoricalView_Previews: PreviewProvider {
    static var previews: some View {
        HistoricalView()
    }
}


