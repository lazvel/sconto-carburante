//
//  ProfileSectionsView.swift
//  ScontoCarburanteDemo
//
//  Created by lazar.velimirovic on 12/3/21.
//

import SwiftUI

struct ProfileSectionsView: View {
    @Binding var isShown: Bool
    
    var body: some View {
        VStack(spacing: 20) {
            Section {
                HStack {
                    VStack(alignment: .leading, spacing: 10) {
                        Text("**ANAGRAFICA**")
                        Text("**SESSO:** Maschio")
                        Text("**DATA DI NASCITA:** 15/02/1989")
                        Text("**Comune DI NASCITA:** Milano")
                    }
                    .padding(.leading)
                    Spacer()
                }
                .foregroundColor(.darkBlue)
                .frame(width: UIScreen.main.bounds.width - 40)
                .padding(.vertical)
                .background(.white)
            }
            .cornerRadius(7)
            
            Section {
                HStack {
                    VStack(alignment: .leading,spacing: 10) {
                        Text("**RUOLO ATTIVO**")
                        Text("Stai navigando come **Cittadino**")
                    }
                    .padding(.leading)
                    Spacer() // ovde slika umesto spacer-a
                    Image("profileCar")
                        .offset(x: 10)
                }
                .foregroundColor(.darkBlue)
                .frame(width: UIScreen.main.bounds.width - 40)
                .padding(.vertical)
                .background(.white)
            }
            .cornerRadius(7)
            
            Section {
                HStack {
                    VStack(alignment: .leading,spacing: 10) {
                        Text("**NUOVA TARGA**")
                        Text("Richiedi una nuova targa")
                    }
                    .padding(.leading)
                    Spacer() // ovde slika umesto spacer-a
                    Button {
                        
                    } label: {
                        HStack {
                            Image("goTo")
                            Text("Vai al sito")
                                .font(.title3)
                                .foregroundColor(.greenMain)
                        }
                        .padding(.trailing)
                    }
                        
                }
                .foregroundColor(.darkBlue)
                .frame(width: UIScreen.main.bounds.width - 40)
                .padding(.vertical)
                .background(.white)
            }
            .cornerRadius(7)
            
            // Fascia - the little
            Section {
                HStack {
                    VStack(alignment: .leading,spacing: 10) {
                        Text("**FASCIA DI SCONTO**")
                    }
                    .padding(.leading)
                    Spacer() // ovde slika umesto spacer-a
                    Image("discountRange")
                        .padding(.trailing)
                }
                .foregroundColor(.darkBlue)
                .frame(width: UIScreen.main.bounds.width - 40)
                .padding(.vertical)
                .background(.white)
            }
            .cornerRadius(7)
            
            // security
            Section {
                HStack {
                    Image("security")
                        .padding(.leading)
                    Spacer()
                    
                    Text("Termini di utilizzo")
                        .font(.subheadline.weight(.heavy))
                    
                    Spacer()
                    
                    Image("goTo")
                        .renderingMode(.template)
                        .padding(.trailing)
                        .foregroundColor(.darkBlue)
                }
                .foregroundColor(.darkBlue)
                .frame(width: UIScreen.main.bounds.width - 40)
                .padding(.vertical)
                .background(.white)
            }
            .cornerRadius(7)
            
            // lock
            
            Section {
                HStack {
                    Image("lock")
                        .padding(.leading)
                    Spacer()
                    
                    Text("Privacy e cookies")
                        .font(.subheadline.weight(.heavy))
                    
                    Spacer()
                    
                    Image("goTo")
                        .renderingMode(.template)
                        .padding(.trailing)
                        .foregroundColor(.darkBlue)
                }
                .foregroundColor(.darkBlue)
                .frame(width: UIScreen.main.bounds.width - 40)
                .padding(.vertical)
                .background(.white)
            }
            .cornerRadius(7)
            
            Button {
                isShown.toggle()
            } label: {
                HStack {
                    Image("escape")
                    Text("Esci dal profilo")
                        .foregroundColor(.red)
                        .font(.title3.bold())
                }
            }
            .padding(.vertical, 20)
            
        }
    }
}

//struct ProfileSectionsView_Previews: PreviewProvider {
//    static var previews: some View {
//        ProfileSectionsView()
//    }
//}
