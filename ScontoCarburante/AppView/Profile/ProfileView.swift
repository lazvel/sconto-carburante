//
//  ProfileView.swift
//  ScontoCarburanteDemo
//
//  Created by lazar.velimirovic on 12/2/21.
//

import SwiftUI

struct ProfileView: View {
    @State private var isProfile = true
    @State private var alertShown = false
    @State private var secondBtn = true
    
    var body: some View {
        ScrollView {
            ZStack {
                
                VStack {
                    Spacer()
                    
                    HStack(spacing: 20) {
                        Spacer()
                        Image("profileIco")
                        VStack(alignment: .leading, spacing: 10) {
                            Text("Mario Rossi")
                                .font(.title.bold())
                            Text("FASDF34R43F546E")
                                .fontWeight(.bold)
                        }
                        .foregroundColor(.white)
                        .frame(minWidth: 200)
                        
                        Spacer()
                        Spacer()
                    }
                    
                    Spacer()
                    Spacer()
                    Spacer()
                }
                .frame(width: UIScreen.main.bounds.width, height: 200)
                .background(Color.greenMain)
                .navigationBarBackButtonHidden(true)
                .navigationBarItems(leading: NavBarBackButton(isProfile: $isProfile))
                
                if alertShown {
                    customAlertView(shown: $alertShown, alertTitle: "Sei sicuro?", alertDescription: "Uscendo dall'app dovrai reffettuare\n nuoavamente il login per rientrare", buttonTitle: "Si", imgUrl: "alertYellow", secondBtnTitle: "No", haveSecondButton: $secondBtn, isProfileAlert: true)
                        .shadow(color: Color.darkBlue, radius: 100)
                }
            }
            ProfileSectionsView(isShown: $alertShown)
                .offset(y: -100)
        }
        .background(Color.firstToWhite)
    }
}


struct ProfileView_Previews: PreviewProvider {
    static var previews: some View {
        ProfileView()
            .previewDevice("iPhone 8")
    }
}
