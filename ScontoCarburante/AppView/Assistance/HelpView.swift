//
//  HelpView.swift
//  ScontoCarburanteDemo
//
//  Created by lazar.velimirovic on 12/2/21.
//

import SwiftUI

struct HelpView: View {
    @State private var isProfile = true
    @State private var rect: CGRect = CGRect()
    
    var body: some View {
        ScrollView {
            ZStack {
                Image("Group2602")
                
                VStack(spacing: 30) {
                    AskAQuestionView()
                    
                    
                    NewMessageSubview()
                    
                    // make a call
                    ContactNumberView()
                    
                    // FAQ
                    FrequentlyAskedQuestionsView()
                    
                    Spacer()
                    Spacer()
                    
                    FooterInfoView()
                }
                
                
            }
            .navigationBarBackButtonHidden(true)
            .navigationBarItems(leading: NavBarBackButton(isProfile: $isProfile))
        }
        .background(Color.firstToWhite)
    }
}

struct ContactNumberView: View {
    var body: some View {
        VStack {
            HStack(spacing: 20) {
                Image("phone")
                VStack(alignment: .leading, spacing: 5) {
                    Text("Chiama il numero verde")
                        .font(.headline)
                    Text("800.318.318")
                        .font(.title2.bold())
                        .foregroundColor(.greenMain)
                }
            }
            .padding()
            .background(.white)
            .cornerRadius(7)
            .shadow(radius: 50)
            
            Text("Dal lunedi al sabato, dalle 8.00 alle 20.00")
        }
    }
}

struct HelpView_Previews: PreviewProvider {
    static var previews: some View {
        HelpView()
    }
}
