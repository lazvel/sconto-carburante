//
//  FrequentlyAskedQuestionsView.swift
//  ScontoCarburanteDemo
//
//  Created by lazar.velimirovic on 12/2/21.
//

import SwiftUI

struct FrequentlyAskedQuestionsView: View {
    let faqs = FAQ.faqs
    @State private var isToggled = false
    @State private var uid = UUID()
    
    var body: some View {
        VStack {
            Text("FAQ")
                .font(.title.bold())
                .foregroundColor(.greenMain)
//            List {
                Section {
                    ForEach(faqs, id: \.id) { faq in
                        VStack {
                            if isToggled && uid == faq.id {
                                customFaq(frequentlyAsked: faq, isToggled: $isToggled, uid: $uid)
                            } else {
                                HStack {
                                    Text(faq.title)
                                        .font(isToggled && uid == faq.id ? .title3.bold() : .body)
                                        .foregroundColor(isToggled  && uid == faq.id  ? Color.darkBlue : Color.black)
                                    Spacer()

                                    Button {
                                        uid = faq.id
                                        isToggled.toggle()
                                    } label: {
                                        ZStack {
                                            Image(isToggled && uid == faq.id ?  "greenCircle" : "emptyCircle")
                                            Image(systemName: isToggled && uid == faq.id ?  "chevron.up" : "chevron.down")
                                                .foregroundColor(isToggled && uid == faq.id ? Color.white : Color.black)
                                        }
                                    }
                                }
                                .padding()
                                .cornerRadius(7)
                            }
                        }
                        .cornerRadius(7)
                        .background(.white)
                        .padding(.horizontal)
                    }
                }
               // kraj sekcije
                
                
//            }
        }
    }
}

struct customFaq: View {
    let frequentlyAsked: FAQ
    @Binding var isToggled: Bool
    @Binding var uid: UUID
    
    var body: some View {
        HStack {
            Rectangle()
                .frame(width: 7)
                .foregroundColor(.greenMain)
            VStack {
                HStack {
                    Text(frequentlyAsked.title)
                        .font(isToggled ? .title3.bold() : .body)
                        .foregroundColor(isToggled ? Color.darkBlue : Color.black)
                    Spacer()
                    
                    Button {
    //                                    uid = faq.id
                        isToggled.toggle()
                    } label: {
                        ZStack {
                            Image(isToggled ? "greenCircle" : "emptyCircle")
                            Image(systemName: isToggled && uid == frequentlyAsked.id ? "chevron.up" : "chevron.down")
                                .foregroundColor(isToggled && uid == frequentlyAsked.id  ? Color.white : Color.black)
                        }
                    }
                }
                Text(frequentlyAsked.description)
                    .fontWeight(.light)
            }
        }
        .padding()
        .background(Color.greenLight.opacity(0.2))
        .cornerRadius(7)
    }
}

struct FrequentlyAskedQuestionsView_Previews: PreviewProvider {
    static var previews: some View {
        FrequentlyAskedQuestionsView()
    }
}
