//
//  MessageFormView.swift
//  ScontoCarburanteDemo
//
//  Created by lazar.velimirovic on 12/3/21.
//

import SwiftUI

struct MessageFormView: View {
    @Binding var name: String
    @Binding var isCittadino: Bool
    @Binding var isEsercente: Bool
    
    @State private var selectedTipology = ""
    let tipologies = ["Option 1", "Option 2", "Option 3"]
    
    
    var body: some View {
        Form {
            Text("Messagio")
                .frame(alignment: .leading)
                .foregroundColor(.greenMain)
                .font(.title.bold())
                .listRowBackground(Color(uiColor: .clear))
                
            Section {
                TextField("Mario", text: $name)
            } header: {
                Text("Nome")
                    .foregroundColor(.darkBlue)
                    .font(.headline.bold())
            }
            .headerProminence(.increased)
            
            Section {
                TextField("Rossi", text: $name)
            } header: {
                Text("Cognome")
                    .foregroundColor(.darkBlue)
                    .font(.headline.bold())
            }
            .headerProminence(.increased)
            
            Section {
                TextField("FASDF34R4F546E", text: $name)
            } header: {
                Text("Codice Fiscale")
                    .foregroundColor(.darkBlue)
                    .font(.headline.bold())
            }
            .headerProminence(.increased)
            
            // radio btn section
            Section {
                HStack {
                    HStack(spacing: 20)  {
                        Text("Cittadino")
                        Button {
                            isCittadino.toggle()
                        } label: {
                            Image(isCittadino && !isEsercente ? "radioOn" : "radioOff")
                        }
                    }
                    
                    Spacer()
                    Spacer()
                    Spacer()
                    Spacer()
                    
                    HStack(spacing: 20)  {
                        Text("Esercente")
                        Button {
                            isEsercente.toggle()
                        } label: {
                            Image(isEsercente && !isCittadino ? "radioOn" : "radioOff")
                        }
                    }

                }
            } header: {
                Text("Ruolo")
                    .foregroundColor(.darkBlue)
                    .font(.headline.bold())
            }
            .listRowBackground(Color(uiColor: .clear))
            .headerProminence(.increased)
            // end of radio buttons
        
            Section {
                Picker("Dettagli Plafond", selection: $selectedTipology) {
                    ForEach(tipologies, id: \.self) { option in
                        Text(option)
                    }
                }
            } header: {
                Text("Tipologia di richiesta")
                    .foregroundColor(.darkBlue)
                    .font(.headline.bold())
            }
            .headerProminence(.increased)
            
            Section {
                TextEditor(text: $name)
                    .frame(minHeight: 150)
            } header: {
                Text("Messagio")
                    .foregroundColor(.darkBlue)
                    .font(.headline.bold())
            }
            .headerProminence(.increased)
        }
        .onAppear{
            UITableView.appearance().backgroundColor = .clear
        }
    }
}
