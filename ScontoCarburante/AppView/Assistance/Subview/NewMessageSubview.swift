//
//  NewMessageSubview.swift
//  ScontoCarburanteDemo
//
//  Created by lazar.velimirovic on 12/2/21.
//

import SwiftUI

struct NewMessageSubview: View {
    @State private var shouldShow = false
    var body: some View {
        VStack(spacing: 20) {
            Text("Aiuto")
                .font(.title.bold())
                .foregroundColor(.greenMain)
            Text("Contattaci per maggiori informazioni")
                .font(.headline)
            
            Button {
                shouldShow.toggle()
            } label: {
                Text("Inviaci un messagio")
                    .font(.title2.bold())
                    .foregroundColor(.white)
            }
            .frame(width: UIScreen.main.bounds.width - 50)
            .padding()
            .background(Color.greenMain)
            .cornerRadius(7)
            .fullScreenCover(isPresented: $shouldShow) {
                MessageView()
            }
        }
    }
}

struct NewMessageSubview_Previews: PreviewProvider {
    static var previews: some View {
        NewMessageSubview()
    }
}
