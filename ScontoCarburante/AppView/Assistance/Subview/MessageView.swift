//
//  MessageView.swift
//  ScontoCarburanteDemo
//
//  Created by lazar.velimirovic on 12/2/21.
//

import SwiftUI

struct MessageView: View {
    @State private var isProfile = false
    @State private var name = ""
    
    @State private var isCittadino = true
    @State private var isEsercente = false
    
    var body: some View {
        NavigationView {
            ZStack {
                Image("Group2602")
                VStack {
                    MessageFormView(name: $name, isCittadino: $isCittadino, isEsercente: $isEsercente)
                        
                    
                    SendBtnView()
                    
                    Spacer()
                    
                    FooterInfoView()
                }
                
                
            }
            .background(Color.firstToWhite)
            .toolbar {
                ToolbarItemGroup(placement: .navigationBarLeading) {
                    HStack {
                        NavButtonsView(isMessage: true)
                            .padding()
                        Spacer()
                    }
                    .frame(width: UIScreen.main.bounds.width)
                    .background(.white)
                }
            }
            .background(.white)
            
        }
        
    }
    
}

struct SendBtnView: View {
    var body: some View {
        Button {
            
        } label: {
            Text("Invia")
                .padding()
                .font(.title2.bold())
        }
        .frame(width: UIScreen.main.bounds.width - 30)
        .background(Color.greenMain)
        .foregroundColor(.white)
        .cornerRadius(7)
    }
}

struct MessageView_Previews: PreviewProvider {
    static var previews: some View {
        MessageView()
    }
}
