//
//  AskAQuestionView.swift
//  ScontoCarburanteDemo
//
//  Created by lazar.velimirovic on 12/2/21.
//

import SwiftUI

struct AskAQuestionView: View {
    @State private var rect: CGRect = CGRect()
    
    var body: some View {
        HStack(spacing: 30) {
            ZStack {
                Circle()
                    .foregroundColor(Color.buttonBlue)
                    .frame(width: 40)
                Image("messageWhite")
            }

            Text("Vai al questionario per\n valutare questa applicazione")
                .underline(true, color: .buttonBlue)
                .foregroundColor(.white)
                .font(.headline)
            Image("goTo")
                .renderingMode(.template)
                .foregroundColor(.white)
        }
        .frame(width: UIScreen.main.bounds.width, height: 80)
        .background(Color.buttonBlue)
    }
}

struct AskAQuestionView_Previews: PreviewProvider {
    static var previews: some View {
        AskAQuestionView()
    }
}
