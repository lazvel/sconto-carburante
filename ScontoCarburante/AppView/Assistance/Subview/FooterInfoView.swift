//
//  FooterInfoView.swift
//  ScontoCarburanteDemo
//
//  Created by lazar.velimirovic on 12/2/21.
//

import SwiftUI

struct FooterInfoView: View {
    @State private var rect: CGRect = CGRect()
    
    var body: some View {
        HStack {
            Image("logoLombardia")
            
            Rectangle()
                .frame(width: 1, height: 50)
                .foregroundColor(.gray)
            
            VStack(alignment: .leading) {
                Text("Versione: **<codice>**")
                Text("Revisione: **<codice>**")
                Text("©  **Regione Lombardia**")
            }
            .font(.system(size: 14))
        }
        .padding(.vertical, 30)
        .frame(width: UIScreen.main.bounds.width)
        .background(.white)
    }
}

struct FooterInfoView_Previews: PreviewProvider {
    static var previews: some View {
        FooterInfoView()
    }
}
