//
//  SafeView.swift
//  ScontoCarburante
//
//  Created by aleksandar.aleksic on 23.11.21..
//

import SwiftUI

struct SafeView: View {
    
    @State private var alertIsShown: Bool = false
    
    var body: some View {
        ZStack{
            VStack{
                Section{
                    HeaderSafeView()
                }
                .frame(height: 180)
                .frame(maxWidth: .infinity)
                .background(Color.firstToWhite)
                
                Section{
                    UserDetailSafeView()
                        .padding([.leading, .trailing], 20)
                }
                .padding(.top, 25)
                
                Section{
                    Image("grayLine")
                }
                .padding(.top, 25)
                
                Section{
                 Text("Il presente documento e iniato telematicamente dal sottoscritto ai sensi dell'art. 38 DPR 445/2000 e.s.m.i. e art. 65 D.Lgs 82/2005")
                        .font(.system(size: 13.5).italic())
                        .foregroundColor(.darkBlue)
                        .fixedSize(horizontal: false, vertical: true)
                        .padding(EdgeInsets(top: 0,
                                            leading: 25,
                                            bottom: 0,
                                            trailing: 25))
                }
                .padding(.top, 25)
                
                Spacer()
                Spacer()
                
                ButtonFooterSafeView()
                
                Spacer()

            }
            .frame(maxHeight: .infinity)
            
            if alertIsShown{
                if let alertText = DemoData.alerts[0],
                   let buttonTitle = DemoData.buttonTitles["close"]{
                    AlertView(title: alertText.title,
                              description: alertText.description,
                              buttonTitle: buttonTitle,
                              alertType: .alert,
                              alertIsShown: $alertIsShown)
                }
            }
        }
    }
}

struct SafeView_Previews: PreviewProvider {
    static var previews: some View {
        SafeView()
    }
}
