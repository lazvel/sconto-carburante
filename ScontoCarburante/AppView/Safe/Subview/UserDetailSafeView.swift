//
//  UserDetailSafeView.swift
//  ScontoCarburante
//
//  Created by aleksandar.aleksic on 24.11.21..
//

import SwiftUI

struct UserDetailSafeView: View {
    var body: some View {
        VStack(alignment: .leading, spacing: 7){
            Section{
                Text("Rossi, Mario")
                    .font(.body.bold())
                    .foregroundColor(.darkBlue)
                
                Text("RSSMRO78H39X499B")
                    .font(.body)
                    .padding(.bottom, 25)
                    .foregroundColor(.darkBlue)
            }
            
            
            Section{
                Text("Dichiaro:")
                    .font(.body.bold())
                    .foregroundColor(.darkBlue)
                
                Text("di aver letto e di aver compreso l'informativa sul trattamento dei dati personali.")
                    .lineSpacing(5)
                    .font(.system(size: 13.5))
                    .foregroundColor(.darkBlue)
                    .fixedSize(horizontal: false, vertical: true)
            }
        }
        .frame(maxWidth: .infinity)
    }
}

struct UserDetailSafeView_Previews: PreviewProvider {
    static var previews: some View {
        UserDetailSafeView()
    }
}
