//
//  ButtonFooterSafeView.swift
//  ScontoCarburante
//
//  Created by aleksandar.aleksic on 24.11.21..
//

import SwiftUI

struct ButtonFooterSafeView: View {
    
    @State private var goToHP: Bool = false
    
    var body: some View {
        Section{
            HStack{
                Spacer()
                Button("Rifiuta"){
                    
                }
                .font(.system(size: 18, weight: .semibold))
                .frame(width: 120, height: 43)
                .background(.white)
                .foregroundColor(Color.greenMain)
                .border(Color.greenMain, width: 2.0)
                .cornerRadius(5)
                
                Spacer()
                
                Button("Accetta"){
                    goToHP.toggle()
                }
                .font(.system(size: 18, weight: .semibold))
                .frame(width: 120, height: 43)
                .background(Color.greenMain)
                .foregroundColor(.white)
                .cornerRadius(5)
                .fullScreenCover(isPresented: $goToHP) {
                    HPView(selectedCar: 0, notifications: DemoData.notifications)
                }
                
                Spacer()
            }
        }
    }
}

struct ButtonFooterSafeView_Previews: PreviewProvider {
    static var previews: some View {
        ButtonFooterSafeView()
    }
}
