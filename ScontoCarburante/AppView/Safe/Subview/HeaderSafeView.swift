//
//  HeaderSafeView.swift
//  ScontoCarburante
//
//  Created by aleksandar.aleksic on 24.11.21..
//

import SwiftUI

struct HeaderSafeView: View {
    var body: some View {
        VStack(alignment: .leading, spacing: 15){
            Image("logoRegione")
                .padding(.bottom, 25)
            
            Text("Invio istanza")
                .font(.title2.bold())
                .foregroundColor(Color.greenMain)
            
            Text("Accettazione termini di\n utillizzo e informativa privacy")
                .font(.system(size: 13, weight: .semibold))
                .foregroundColor(.darkBlue)
        }
        .frame(maxWidth: .infinity, alignment: .leading)
        .padding(.leading, 30)
    }
}

struct HeaderSafeView_Previews: PreviewProvider {
    static var previews: some View {
        HeaderSafeView()
    }
}
