//
//  HPView.swift
//  ScontoCarburante
//
//  Created by aleksandar.aleksic on 24.11.21..
//

import SwiftUI

struct Notification: Hashable{
    
    var title: String
    var description: String
}

struct HPView: View {
    
    @State var selectedCar: Int
    @State var notifications: [Notification]
    
    var vehicles  = DemoData.vehicles
    
    var body: some View {
        NavigationView{
            ZStack {
                ScrollView{
                    VStack{
                        if notifications.count >= 1{
                            NotificationView(notifications: $notifications)
                        }
                        
                        ChooseVehicleView(vehicles: vehicles)
                        
                        FuelStatusView()
                        
                        RefuelingHistoryView()
                        
                        Spacer()
                    }
                    .toolbar {
                        ToolbarCarburante(hasBackButton: false)
                    }
                    .navigationBarTitleDisplayMode(.inline)
                }
                
                VStack{
                    Rectangle()
                        .fill(Color.greenMain)
                        .frame(height: 2)
                    
                    Spacer()
                    
                    NewRefuelingButtonView()
                        .frame(height: 50)
                }
            }
        }
    }

}

struct HPView_Previews: PreviewProvider {
    static var previews: some View {
        Group {
            HPView(selectedCar: 0, notifications: DemoData.notifications)
                .previewInterfaceOrientation(.portrait)
        }
    }
}
