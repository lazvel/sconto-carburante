//
//  HPEmptyView.swift
//  ScontoCarburante
//
//  Created by aleksandar.aleksic on 26.11.21..
//

import SwiftUI

struct HPEmptyView: View {
    var body: some View {
        NavigationView{
            ZStack{
                VStack{
                    AlertBodyView(title: "Non hai veicoli",
                                  description: "Per poter utilizzare l'App devi aver registrato almeno un veicolo (auto o moto) presso...",
                                  alertType: nil)
                        
                }.toolbar {
                    ToolbarCarburante(hasBackButton: false)
                }
                .navigationBarTitleDisplayMode(.inline)
                
                NavigationBottomLine()
            }
        }
        
    }
}

struct HPEmptyView_Previews: PreviewProvider {
    static var previews: some View {
        HPEmptyView()
    }
}
