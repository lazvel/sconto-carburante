//
//  ChooseVehicleView.swift
//  ScontoCarburante
//
//  Created by aleksandar.aleksic on 26.11.21..
//

import SwiftUI

struct ChooseVehicleView: View {
    
    var vehicles: [Vehicle]
    
    var body: some View {
        VStack{
            
            HStack{
                Text("Veicoli").bold()
                    .foregroundColor(Color.darkBlue)
                
                Spacer()
                
                NavigationLink(destination: VehicleView()) {
                    HStack{
                        Text("Gestisci")
                            .foregroundColor(Color.darkBlue)
                        Image(systemName: "arrow.right")
                            .foregroundColor(Color.darkBlue)
                    }
                }
            }
            .padding(EdgeInsets(top: 10, leading: 20, bottom: 5, trailing: 20))
            
            SegmentedControlHP(vehicles: vehicles)
                .frame(maxWidth: .infinity)
                .scaleEffect(0.9)
                .padding(.leading, -20)
        }
        .background(Color.firstToWhite)
    }
}

//struct ChooseVehicleView_Previews: PreviewProvider {
//    static var previews: some View {
//        ChooseVehicleView()
//    }
//}
