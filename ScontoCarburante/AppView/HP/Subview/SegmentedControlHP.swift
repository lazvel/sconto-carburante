//
//  SegmentedControlHP.swift
//  ScontoCarburante
//
//  Created by aleksandar.aleksic on 24.11.21..
//

import SwiftUI

struct SegmentedControlHP: View {

    var vehicles: [Vehicle]
    
    @State var selectedVehicle: Int = 0
    
    var body: some View {
        HStack(spacing: 7){
            
            ForEach(0...(vehicles.count - 1), id: \.self){
                            
            SegmentedControlHPCell(selectedVehicle: $selectedVehicle,
                            serialNumber: $0,
                            vehicle: vehicles[$0])
            }
            Spacer()
        }
        .padding(EdgeInsets(top: 2,
                            leading: 25,
                            bottom: 2,
                            trailing: 25))
    }
    

}

struct SegmentedControlHP_Previews: PreviewProvider {
    static var previews: some View {
        SegmentedControlHP(vehicles: DemoData.vehicles)
    }
}
