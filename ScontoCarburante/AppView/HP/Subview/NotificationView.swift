//
//  NotificationView.swift
//  ScontoCarburante
//
//  Created by aleksandar.aleksic on 26.11.21..
//

import SwiftUI

struct NotificationView: View {
    
    @Binding var notifications: [Notification]
    
    var body: some View {
        VStack{
            ScrollView(.horizontal){
                HStack{
                    ForEach(notifications, id:\.title){ notif in
                        VStack(alignment: .leading, spacing: 10){
                            HStack{
                                Text(notif.title)
                                    .padding(EdgeInsets(top: 0,
                                                        leading: 12,
                                                        bottom: 1,
                                                        trailing: 12))
                                    .font(.body.bold())
                                    .foregroundColor(Color.textGray)
                                
                                Spacer()
                                
                                Button{
                                    notifications = []
                                }label: {
                                    Image("closeBtn")
                                        .resizable()
                                        .frame(width: 12, height: 12)
                                }
                                .padding(.trailing, 15)
                                .padding(.top, -5)
                                .onTapGesture {
                                    removeItems(object: notif)
                                }
                            }
                            .padding(.top, 12)
                            
                            Text(notif.description)
                                .padding(EdgeInsets(top: 0,
                                                    leading: 12,
                                                    bottom: 1,
                                                    trailing: 12))
                                .font(.system(size: 15.5, weight: .light))
                                .foregroundColor(Color.textGray)
                            Spacer()
                        }
                        .frame(width: 300, height: 120)
                        .background(Color.white)
                        .cornerRadius(15)
                        .padding(EdgeInsets(top: 0, leading: 7, bottom: 0, trailing: 7))
                    }
                }
                .padding(.leading, 20)
            }
            .frame(height:120)
        }
        .frame(height: 150)
        .background(Color.firstToWhite)
    }
    
    private func removeItems(object: Notification){
        guard let index = notifications.firstIndex(of: object) else {return}
        notifications.remove(at: index)
        DemoData.notifications = []
    }
}


//struct NotificationView_Previews: PreviewProvider {
//    static var previews: some View {
//        NotificationView(notifications: DemoData.notifications)
//    }
//}
