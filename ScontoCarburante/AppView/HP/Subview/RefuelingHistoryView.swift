//
//  RefuelingHistoryView.swift
//  ScontoCarburante
//
//  Created by aleksandar.aleksic on 26.11.21..
//

import SwiftUI

struct RefuelingHistoryView: View {
    
    @State var showVeicoli: Bool = false
    
    var body: some View {
        VStack{
            HStack{
                Text("Storico")
                    .font(.title3)
                    .bold()
                
                Spacer()
                
                NavigationLink(destination: HistoricalView()) {
                                        HStack{
                                            Text("Dettagli")
                                            Image(systemName: "arrow.right")
                                        }
                                        .foregroundColor(Color.darkBlue)
                }

            }
            .padding(EdgeInsets(top: 10,
                                leading: 20,
                                bottom: 5,
                                trailing: 20))
            
            HStack{
                VStack(alignment: .leading, spacing: 8){
                    Text("2")
                        .padding(.leading, 4)
                        .padding(.top, 10)
                        .font(.title3.bold())
                    Text("Rifornimenti effettuati nell'ultimo mese")
                        .padding(.leading, 4)
                        .font(.system(size: 16, weight: .light))
                    Spacer()
                }
                .padding(.leading, 8)
                .frame(width: 160, height:  120)
                .background(Color.firstToWhite)
                .cornerRadius(20)
                .foregroundColor(Color.darkBlue)
                
                VStack(alignment: .leading, spacing: 8){
                    Text("18")
                        .padding(.leading, 4)
                        .padding(.top, 10)
                        .font(.title3.bold())
                    Text("Rifornimenti effettuati nell 2020")
                        .padding(.leading, 4)
                        .font(.system(size: 16, weight: .light))
                    Spacer()
                }
                .padding(.leading, 8)
                .frame(width: 160, height:  120)
                .background(Color.firstToWhite)
                .cornerRadius(20)
                .foregroundColor(Color.darkBlue)
            }
        }
        .padding(.top, 15)
        .padding(.bottom, 70)
    }
}

struct RefuelingHistoryView_Previews: PreviewProvider {
    static var previews: some View {
        RefuelingHistoryView()
    }
}
