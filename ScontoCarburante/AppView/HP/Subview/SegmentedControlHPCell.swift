//
//  SegmentedControlHPCell.swift
//  ScontoCarburante
//
//  Created by aleksandar.aleksic on 24.11.21..
//

import SwiftUI

struct SegmentedControlHPCell: View {
    
    @Binding var selectedVehicle: Int
    @State var serialNumber: Int
    
    var vehicle: Vehicle
    
    var body: some View {
        VStack{
            Image(selectImage(serialNumber: serialNumber))
                .frame(width: 60, height: 50)
            Text("\(vehicle.name)").tag(vehicle.id)
                .foregroundColor(getColor(serialNumber: serialNumber))
                .font(.system(size: 16))
                .frame(maxWidth: .infinity)
            if selectedVehicle == serialNumber{
                Rectangle()
                    .fill(Color.greenMain)
                    .frame(width: 70, height: 2)
            }
        }
        .frame(width: 70)
        .onTapGesture {
            selectedVehicle = serialNumber
        }
    }
    
    private func selectImage(serialNumber: Int) -> String{
        if selectedVehicle == serialNumber{
            return "\(vehicle.type)Green"
        }
        return "\(vehicle.type)Blue"
    }
    
    private func getColor(serialNumber: Int) -> Color{
        if selectedVehicle == serialNumber{
            return Color.greenMain
        }
        return Color.darkBlue
    }
}

struct SegmentedControlHPCell_Previews: PreviewProvider {
    static var previews: some View {
        SegmentedControlHPCell(selectedVehicle: .constant(0),
                               serialNumber: 0,
                               vehicle: DemoData.vehicles[0])
    }
}
