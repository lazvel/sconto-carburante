//
//  NewRefuelingView.swift
//  ScontoCarburante
//
//  Created by aleksandar.aleksic on 26.11.21..
//

import SwiftUI

struct NewRefuelingButtonView: View {
    
    @State private var showNewRefueling: Bool = false
    
    var body: some View {
        HStack(alignment: .center){
                Button{
                    showNewRefueling.toggle()
                }label: {
                    Text("Nuovo rifornimento")
                        .font(.title2)
                        .bold()
//                        .padding(.top, 5)
                }
                .fullScreenCover(isPresented: $showNewRefueling) {
                    NewRefuelingView()
                }
            }
            .frame(maxWidth: .infinity)
            .frame(height: 50)
            .background(Color.greenMain)
            .foregroundColor(.white)
            .ignoresSafeArea()
        
    }
}

struct NewRefuelingButtonView_Previews: PreviewProvider {
    static var previews: some View {
        NewRefuelingButtonView()
    }
}
