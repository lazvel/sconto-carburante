//
//  FuelStatusView.swift
//  ScontoCarburante
//
//  Created by aleksandar.aleksic on 26.11.21..
//

import SwiftUI

struct FuelStatusView: View {
    var body: some View {
        VStack{
            HStack{
                Image(systemName: "calendar")
                    .padding(.leading, 6)
                    .foregroundColor(Color.darkBlue)
                HStack(spacing: 0){
                    Text("Ultimo rifornimento: ")
                        .foregroundColor(Color.darkBlue)
                        .font(.system(size: 15))
                    
                    Text("\("28/09/2020 - 18:30")")
                        .bold()
                        .padding(.trailing, 15)
                        .foregroundColor(Color.darkBlue)
                        .font(.system(size: 15))
                }
            }
            .frame(height: 30)
            .background(Color.firstToWhite)
            .cornerRadius(5)
            .padding(EdgeInsets(top: 8, leading: 8, bottom: 8, trailing: 8))
   
            
            VStack(alignment: .leading, spacing: 15){
                Section{
                Text("Plafond ").bold()
                        .font(.title3)  +
                Text("| Litri a disposizione")
                }
                .padding(.leading, 18)
                .foregroundColor(Color.darkBlue)
                
                HStack{
                    Spacer()
                    
                    VStack(alignment: .leading, spacing: 0){
                        Text("Oggi")
                            .font(.title2)
                            .bold()
                            .padding(.bottom, 2)
                        
                        Rectangle()
                            .fill(Color.darkBlue)
                            .frame(width: 50, height: 4)
                            .cornerRadius(10)
                            .padding(.bottom, 10)
                        
                        Text("38 L")
                            .font(.title2)
                            .bold()
                        
                        Text("disponibili")
                            .padding(.bottom, 10)
                        
                        DonutChart(gasoline: 20.0, gasCostType: .daily)
                            .scaleEffect(0.8)
                    }
                    .foregroundColor(Color.textGray)
                    
                    Spacer()
                    Spacer()
                    
                    VStack(alignment: .leading, spacing: 0){
                        Text("Mese corrente")
                            .font(.title2)
                            .bold()
                            .padding(.bottom, 2)
                        
                        Rectangle()
                            .fill(Color.greenLight)
                            .frame(width: 50, height: 4)
                            .cornerRadius(10)
                            .padding(.bottom, 10)
                        
                        Text("180 L")
                            .font(.title2)
                            .bold()
                        
                        Text("disponibili")
                            .padding(.bottom, 10)
                        
                        DonutChart(gasoline: 125, gasCostType: .mountly)
                            .scaleEffect(0.8)
                    }
                    .foregroundColor(Color.textGray)
                    
                    Spacer()
                }
            }
            .padding(.top, 20)
            .padding(.leading, 10)
        }
    }
}


struct FuelStatusView_Previews: PreviewProvider {
    static var previews: some View {
        FuelStatusView()
    }
}
