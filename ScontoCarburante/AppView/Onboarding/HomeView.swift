//
//  HomeView.swift
//  ScontoCarburanteDemo
//
//  Created by lazar.velimirovic on 11/19/21.
//

import SwiftUI

struct HomeView: View {
    @State private var titles = ["Scopri il Servizio", "Sei un cittadino?", "Sei registrato al servizio?", "Sei un esercente?", "Inizia subito!"]
    @State private var descriptions = ["Da oggi Cittadini ed Esercenti vivranno una\nnuova esperienza di rifornimento", "Usufruisci di uno sconto sul\n rifornimento di benzina e gasolio", "Monitorare i consumi e i plafond a\n disposizione comodamente dal tuo\n smarthphone", "Traccia e monitora rifornimenti\n erogati, tieni aggiornati i prezzi dei\n singoli prodotti", "Se non se iscritto al servizio scopri come\nregistrarti sul sito\n **www.scontocarburante.regionelombardia.it**\n Oppure"]
    @State private var boldedAdditional = ["Accedi allo sconto Carburante", "Monitora il Plafond", "Semplifica il rifornimento"]
    @State private var btnTitle = "Continua"
    
    var additional: String? {
        if textCount == 1 {
            return boldedAdditional[0]
        } else if textCount == 2 {
            return boldedAdditional[1]
        } else if textCount == 3 {
            return boldedAdditional[2]
        } else {
            return nil
        }
    }
    
    @State private var banner = "MaskGroup"
    @State private var bannerCount = 1
    @State private var textCount = 0

    @State private var shouldHide = false
    @State private var shouldShow = false
    var body: some View {
        ZStack {
            Image("Group2602")
                .position(x: 200, y: 150)
            VStack {
                Spacer()
                
                // upper view body
                VStack {
                    Image("Group")
                        .resizable()
                        .scaledToFit()
                        .frame(width: 150, height: 50, alignment: .center)
                    Spacer()
                    
                    Image("\(banner + String(bannerCount))")
                        .scaledToFit()
                        .frame(maxWidth: 300, maxHeight: 300)
                    
                    Text(titles[textCount])
                        .font(.title.weight(.bold))
                        .fixedSize(horizontal: false, vertical: true)
                        .foregroundColor(.primary)
                        .padding(.bottom, 5)
                        .multilineTextAlignment(.center)
                    Text(additional ?? "")
                        .font(.title3.weight(.semibold))
                        .foregroundColor(.primary)
                        .padding(.bottom, 5)
                    Text(descriptions[textCount])
                        .multilineTextAlignment(.center)
                        .foregroundColor(Color(red: 0.4, green: 0.4, blue: 0.4))
                        .fixedSize(horizontal: false, vertical: true)
                }
                
                Spacer(minLength: 50)
                
                
                // buttons stack
                VStack {
                    Button(shouldHide ? "Accedi" : "Continua") {
                        btnTapped()
                        if btnTitle == "Accedi" {
                            shouldShow = true
                        }
                    }
                    .frame(width: UIScreen.main.bounds.width - 40, height: 50, alignment: .center)
                    .font(.title2.bold())
                    .background(Color(red: 0.2, green: 0.6, blue: 0.1))
                    .foregroundColor(.white)
                    .cornerRadius(6)
                    .fullScreenCover(isPresented: $shouldShow) {
                        TermsAndCondtionsView()
                    }
                    Button("Salta") { }
                    .frame(width: 100)
                    .padding(.top, 20)
                    .font(.title2.bold())
                    .foregroundColor(.secondary)
                    .opacity(shouldHide ? 0 : 1)
                }
                
                Spacer(minLength: 30)
            }
        }
    }
    
    func btnTapped() {
        if bannerCount == 5 && textCount == 4 {
            btnTitle = "Accedi"
        } else if textCount == 3 {
            shouldHide = true
            bannerCount += 1
            textCount += 1
        } else {
            bannerCount += 1
            textCount += 1
        }
    }
}

struct ContentView_Previews: PreviewProvider {
    static var previews: some View {
        HomeView()
            .previewDevice("iPhone 13 Pro")
.previewInterfaceOrientation(.portrait)
    }
}

