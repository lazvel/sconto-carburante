//
//  NavBarBackButton.swift
//  ScontoCarburante
//
//  Created by lazar.velimirovic on 12/15/21.
//

import SwiftUI

struct NavBarBackButton: View {
    @Environment(\.presentationMode) var presentationMode: Binding<PresentationMode>
    @Binding var isProfile: Bool
    var isMessagge = false
    
    var body: some View {
        VStack {
            Button(action: {
                self.presentationMode.wrappedValue.dismiss()
            }) {
                HStack(spacing: 120){
                    HStack {
                        Image("arrowLeft") // set image here
                            .aspectRatio(contentMode: .fit)
                        if !isMessagge {
                            Text(isProfile ? "Indietro" : "Torna alla home")
                                .foregroundColor(.gray)
                                .font(.title3.bold())
                        } else {
                            Text("Torna ad Aiuto")
                                .foregroundColor(.gray)
                                .font(.title3.bold())
                        }
                    }
                    
                    
                    if !isProfile {
                        NavButtonsView()
                    } else {
                        Spacer(minLength: 80)
                    }
                    
                }
            }
            Rectangle()
                .frame(width: UIScreen.main.bounds.width, height: 3)
                .foregroundColor(Color.greenMain)
        }
        .padding(.top)
        .background(.white)
    }
}

//struct NavBarBackButton_Previews: PreviewProvider {
//    static var previews: some View {
//        NavBarBackButton()
//    }
//}
