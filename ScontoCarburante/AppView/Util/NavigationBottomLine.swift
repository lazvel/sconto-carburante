//
//  NavigationBottomLine.swift
//  ScontoCarburante
//
//  Created by aleksandar.aleksic on 26.11.21..
//

import SwiftUI

struct NavigationBottomLine: View {
    
    var lineColor: Color?
    
    var body: some View {
        VStack{
            Rectangle()
                .fill(lineColor != nil ? lineColor! : Color.greenMain)
                .frame(height: 2)
            Spacer()
        }
    }
}

struct NavigationBottomLine_Previews: PreviewProvider {
    static var previews: some View {
        NavigationBottomLine()
    }
}
