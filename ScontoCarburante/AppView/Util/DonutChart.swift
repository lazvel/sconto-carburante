//
//  DonutChart.swift
//  ScontoCarburante
//
//  Created by aleksandar.aleksic on 24.11.21..
//

import SwiftUI

enum GasolinCostType: Int{
    case daily = 50
    case mountly = 200
}

struct DonutChart: View {
    
    var gasoline: CGFloat
    var gasCostType: GasolinCostType
    
    var body: some View {
        VStack {
            ZStack{
                            
                Circle()
                    .trim(from: calculate() , to: 100 )
                    .stroke(Color.firstToWhite, lineWidth: 25)
                    .frame(width: 120, height: 120)
                    .rotationEffect(Angle(radians: 4.7))
                
                Circle()
                    .trim(from: 0, to: calculate())
                    .stroke(getColorForDonut(),
                            style: StrokeStyle(lineWidth: 25, lineCap: .round))
                    .frame(width: 120, height: 120)
                    .rotationEffect(Angle(radians: 4.7))
                
                Text(totalGas())
                    .foregroundColor(Color.darkBlue)
            }
        }
    }
    
    private func getColorForDonut() -> Color{
        return gasCostType == .daily ? Color.darkBlue : Color.greenLight
    }
    
    private func calculate() -> CGFloat{
        return gasCostType == .daily ? (gasoline / 50) : (gasoline / 200)
    }
    
    private func totalGas() -> String {
        return gasCostType == .daily ? "TOT. \n 50 L" : "TOT. \n 200 L"
    }
}

struct DonutChart_Previews: PreviewProvider {
    static var previews: some View {
        DonutChart(gasoline: 94, gasCostType: .mountly)
    }
}
