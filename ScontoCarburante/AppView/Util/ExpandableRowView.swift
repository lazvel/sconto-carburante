//
//  ExpandableRowView.swift
//  ScontoCarburanteDemo
//
//  Created by lazar.velimirovic on 12/2/21.
//

import SwiftUI

struct ExpandableRowView: View {
    let transaction: FuelHistory
    
    var body: some View {
        VStack(spacing: 20) {
            VStack(alignment: .leading) {
                Text("Codice punto vendita: **\(transaction.pumpStationCode)**")
                Text("Prezzo al litro: **\(transaction.litrePrice)**")
                Text("Litri erogati: **\(transaction.litresFueled)**")
                Text("Lordo: **\(transaction.gross)**")
            }
            
            VStack(alignment: .leading) {
                Text("Sconto €/L: **\(transaction.discountPerLitre)**")
                Text("Prezzo al litro scontato: **\(transaction.discountLitrePrice)**")
                Text("Litri scontati: **\(transaction.discountedLitres)**")
                Text("Sconto: **\(transaction.discount)**")
            } .padding(.trailing, 20)
            
            VStack(alignment: .leading)   {
                Text("Importo da pagare: **\(transaction.amountToBePaid)**")
                Text("Plafond residuo: **\(transaction.remainingCeiling)**")
                Text("Data del rifornimento: **\(transaction.refuelingDate)**")
                Text("Orario del rifornimento: **\(transaction.refuelingTime)**")
            }

        }
        .foregroundColor(Color.darkBlue)
    }
}

struct ExpandableRowView_Previews: PreviewProvider {
    static var previews: some View {
        ExpandableRowView(transaction: FuelHistory.transactions.first!)
    }
}
