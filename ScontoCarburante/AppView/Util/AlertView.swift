//
//  AlertView.swift
//  ScontoCarburante
//
//  Created by aleksandar.aleksic on 22.11.21..
//

import SwiftUI

struct AlertView: View {
    
//    @Environment(\.presentationMode) private var presentationMode
    
    var title: String
    var description: String
    var buttonTitle: String?
    var alertType: AlertType
    
    var alternativeView: AnyView?
    
    @Binding var alertIsShown: Bool
    
    var body: some View {
            VStack{
                
                Spacer()
                
                VStack{
                    
                    AlertBodyView(title: title,
                                  description: description,
                                  alertType: alertType)
                    
                    if alternativeView != nil{
                        alternativeView!
                    }else{
                        Button(buttonTitle!){
                            alertIsShown.toggle()
                        }
                        .cornerRadius(10)
                        .font(.title2.bold())
                        .frame(width: 160, height: 50)
                        .background(Color.greenMain)
                        .foregroundColor(.white)
                        .padding(.bottom, 50)
                    }
                    
                }
                .frame(width: UIScreen.screenWidth * 0.85,
                       height: UIScreen.screenHeight * 0.60,
                       alignment: .center)
                .background(.white)
                .cornerRadius(12)
                .clipped()
                
                Spacer()
            }
            .frame(width: UIScreen.screenWidth,
                   height: UIScreen.screenHeight,
                   alignment: .center)
            .background(Color.black.opacity(0.3))
    }
    
}

struct AlertView_Previews: PreviewProvider {
    static var previews: some View {
        AlertView(title: "Alert",
                  description: "Demo data for testing porpuse.",
                  buttonTitle: "OK",
                  alertType: .warrning,
                  alertIsShown: .constant(false))
    }
}

struct AlertBodyView: View {
    
    var title: String
    var description: String
    var alertType: AlertType?
    
    var body: some View {
        VStack{
            Image(imageNameForAlert())
                .padding(.top, 20)
            
            VStack(alignment: alertType != nil ? .center : .leading){
                Text(title)
                    .font(.title.bold())
                    .padding(.top, isSmallScreen() ? 15 : 50)
                    .foregroundColor(.darkBlue)
                    .padding(.leading, alertType != nil ? 0 : 20)
                    .fixedSize(horizontal: false, vertical: true)
                    
                
                Text(description)
                    .font(.system(size: alertType != nil ? 14 : 15))
                    .multilineTextAlignment(alertType != nil ? .center : .leading)
                    .fixedSize(horizontal: false, vertical: true)
                    .foregroundColor(.darkBlue)
                    .padding(20)
            }
            
            Spacer()
        }
    }
    
    private func imageNameForAlert() -> String{
        switch alertType {
        case .alert:
            return "alertRed"
        case .warrning:
            return "alertYellow"
        case .success:
            return "alertGreen"
        case .none:
            return "alertRed"
        }
    }
    
    private func isSmallScreen() -> Bool{
        return UIScreen.screenHeight < 570 ? true : false
    }
}
