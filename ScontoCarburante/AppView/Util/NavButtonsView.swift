//
//  NavButtonsView.swift
//  ScontoCarburante
//
//  Created by lazar.velimirovic on 12/15/21.
//

import SwiftUI

struct NavButtonsView: View {
    @State private var isPresented = false
    @State private var isProfilePresented = false
    var isMessage = false
    
    var body: some View {
        HStack {
            if isMessage {
                NavigationLink {
                    HelpView()
                } label: {
                    HStack {
                        Image("arrowLeft") // set image here
                            .aspectRatio(contentMode: .fit)
                        
                        Text("Torna ad Aiuto")
                            .foregroundColor(.gray)
                            .font(.title3.bold())
                        
//                        Spacer()
                    }
                }
            } else {
                NavigationLink {
                    HelpView()
                } label: {
                    VStack {
                        Image(systemName: "questionmark.circle.fill")
                            .renderingMode(.template)
                            .foregroundColor(Color.greenMain)
                        Text("Aiuto")
                            .font(.system(size: 12))
                            .fontWeight(.semibold)
                            .foregroundColor(.darkBlue)
                    }
                }
                
                NavigationLink {
                    ProfileView()
                } label: {
                    VStack {
                        Image(systemName: "person.circle.fill")
                            .renderingMode(.template)
                            .foregroundColor(Color.greenMain)
                        Text("Profilo")
                            .font(.system(size: 12))
                            .fontWeight(.semibold)
                            .foregroundColor(.darkBlue)
                    }
                }
            }
        }
    }
}

struct NavButtonsView_Previews: PreviewProvider {
    static var previews: some View {
        NavButtonsView()
    }
}
