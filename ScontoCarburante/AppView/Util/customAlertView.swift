//
//  customAlertView.swift
//  ScontoCarburanteDemo
//
//  Created by lazar.velimirovic on 12/1/21.
//

import SwiftUI

struct customAlertView: View {
    @Binding var shown: Bool
    
    var alertTitle = "Attenzione"
    var alertDescription = "Per utilizzare l'app, e necessario\naccettare sia termini di utilizzo sia\nl'informativa sulla privacy."
    var buttonTitle = "Chiudi"
    var imgUrl = "redWarning"
    var secondBtnTitle = ""

    @State private var eliminated = false
    @Binding var haveSecondButton: Bool
    
    var isProfileAlert = false
    
    var isSecondButton: Bool {
        let second = haveSecondButton
        
        return second
    }
    
    var isEliminated: Bool {
        let elimin = eliminated
        
        return elimin
    }
    
    var body: some View {
        ZStack {
            Color.white
            VStack {
                Image(imgUrl)
                    .frame(width: 60, height: 60, alignment: .center)
                    .padding(.top, 30)
                Text(alertTitle)
                    .font(.title.bold())
                    .foregroundColor(Color.darkBlue)
                    .padding(.vertical, 20)
                
                if alertDescription == "" {
                    Text(alertDescription)
                        .font(.system(size: 20).weight(.regular))
                        .multilineTextAlignment(.center)
                        .hidden()
                } else {
                    Text(alertDescription)
                        .font(.system(size: 20).weight(.regular))
                        .multilineTextAlignment(.center)
                    Spacer()
                }
                
                if haveSecondButton {
                    VStack() {
                        Button(buttonTitle) {
                            shown.toggle()
//                            haveSecondButton.toggle()
                            eliminated.toggle()
                        }
                        .frame(width: 150, height: 50, alignment: .center)
                        .font(.title2.bold())
                        .background(isProfileAlert ? Color.greenMain : Color.red)
                        .foregroundColor(.white)
                        .cornerRadius(8)
                        .padding(.top, alertDescription == "" ? 20 : 0)
                        
                        Button(secondBtnTitle) {
                            shown.toggle()
                        }
                        .frame(width: 150, height: 50, alignment: .center)
                        .font(.title2.bold())
                        .background(.white)
                        .foregroundColor(Color.greenMain)
                        .overlay(
                            RoundedRectangle(cornerRadius: 8)
                                .stroke(Color.greenMain, lineWidth: 4)
                        )
                    }.padding(.bottom, 30)
                    
                    if isEliminated {
                        customAlertView(shown: $shown, alertTitle: "Veicolo eliminato correttamente", alertDescription: "", buttonTitle: "Ok grazie", imgUrl: "success", haveSecondButton: $haveSecondButton)
                        .shadow(color: Color.gray, radius: 50)
                    }
                } else {
                    Button(buttonTitle) {
                        shown.toggle()
                    }
                    .frame(width: 150, height: 50, alignment: .center)
                    .font(.title2.bold())
                    .background(Color.greenMain)
                    .foregroundColor(.white)
                    .cornerRadius(8)
                    .padding(.bottom, 30)
                    .padding(.top, alertDescription == "" ? 20 : 0)
                }
                
            }
        }
        .frame(width: UIScreen.main.bounds.width - 30, height: 400, alignment: .topLeading)
        .cornerRadius(10)
        .padding(.bottom, 100)
    }
}

//struct CustomAlertView_Previews: PreviewProvider {
//    static var previews: some View {
//        customAlertView()
//    }
//}
