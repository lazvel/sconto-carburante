//
//  ToolbarCarburante.swift
//  ScontoCarburante
//
//  Created by aleksandar.aleksic on 26.11.21..
//

import SwiftUI

struct ToolbarCarburante: View{
    
    var hasBackButton: Bool
    var backButtonTitle: String?
    var backButtonFun: (()->())?
    
    @State private var openAssistance: Bool = false
  
    var body: some View{
        VStack{
            
        }
        .toolbar{
            ToolbarItemGroup(placement: ToolbarItemPlacement.navigationBarLeading) {
                if hasBackButton{
                    HStack{
                        Image(systemName: "arrow.left")
                            .frame(width: 25, height: 25)
                        Text(backButtonTitle!).bold()
                    }
                    .foregroundColor(.textGray)
                    .onTapGesture {
                        backButtonFun!()
                    }
                }else{
                    Text("Sconto Carburante")
                        .font(.system(size: 18, weight: .semibold))
                        .foregroundColor(Color.darkBlue)
                }
            }
            
            ToolbarItemGroup(placement: ToolbarItemPlacement.navigationBarTrailing){
                HStack{
                    NavigationLink(destination: HelpView()) {
                        ToolbarButton(buttonTitle: "Aiuto",
                                      buttonImageName: "questionmark.circle.fill")
                    }
                    
                    
                    NavigationLink(destination: ProfileView()) {
                        ToolbarButton(buttonTitle: "Profilo",
                                      buttonImageName: "person.circle.fill")
                    }
                }
            }
        }
    }
    
    private func ToolbarButton(buttonTitle: String,
                               buttonImageName: String) -> AnyView{
        return AnyView(
            VStack(spacing: 1){
                Image(systemName: buttonImageName)
                    .resizable()
                    .frame(width: 20, height: 20)
                    .foregroundColor(Color.greenMain)
                Text(buttonTitle)
                    .font(.system(size: 14))
                    .foregroundColor(Color.darkBlue)
                    .bold()
            }
            .padding(.bottom, 1))
    }
}


struct ToolbarCarburante_Previews: PreviewProvider {
    static var previews: some View {
        ToolbarCarburante(hasBackButton: false)
    }
}
