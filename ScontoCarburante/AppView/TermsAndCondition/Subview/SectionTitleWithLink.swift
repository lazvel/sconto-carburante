//
//  SectionTitleWithLink.swift
//  ScontoCarburante
//
//  Created by aleksandar.aleksic on 22.11.21..
//

import SwiftUI

struct SectionTitleWithLink: View {
    
    var title: String
    var url: String
    
    var body: some View{
        HStack{
            Spacer()
            
            Text(title)
                .font(.title3)
                .fontWeight(.semibold)
                .foregroundColor(.darkBlue)
            
            Spacer()
            
            Link(destination: URL(string: url)!) {
                Image("hyperlink")
                    .frame(width: 25, height: 25)
            }
            
            
            Spacer()
        }
        .padding(EdgeInsets(top: 20, leading: 0, bottom: 10, trailing: 0))
    }
}
