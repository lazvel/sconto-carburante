//
//  Checkbox.swift
//  ScontoCarburante
//
//  Created by aleksandar.aleksic on 22.11.21..
//

import SwiftUI

struct Checkbox: View{
    
    @Binding var value: Bool
    var textMessage: String
    
    var body: some View{
        HStack{
            Image(value ? "checkboxFill" : "checkboxEmpty")
                .resizable()
                .frame(width: 30, height: 30)
                .padding(EdgeInsets(top: -35, leading: 0, bottom: 0, trailing: 5))
                .foregroundColor(Color.greenMain)
                .onTapGesture {
                    value.toggle()
                }
            
            Text(textMessage)
                .frame(width: 250)
                .font(.system(size: 18))
                .foregroundColor(.darkBlue)
                .fixedSize(horizontal: false, vertical: true)
        }
        .frame(maxWidth: .infinity)
        .padding(EdgeInsets(top: 0, leading: 25, bottom: 0, trailing: 25))
    }
    

}

struct Checkbox_Previews: PreviewProvider {
    static var previews: some View {
        Checkbox(value: .constant(true), textMessage: "Test test")
    }
}
