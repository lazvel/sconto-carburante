//
//  ButtonTermsAndCondition.swift
//  ScontoCarburante
//
//  Created by aleksandar.aleksic on 22.11.21..
//

import SwiftUI

struct ButtonTermsAndCondition: View{
    
    @Binding var privacy: Bool
    @Binding var terms: Bool
    
    @State private var checked = false
    
    var body: some View{
        
        HStack{
            Spacer()
            Button(DemoData.buttonTitles["goOn"]!){
                checked.toggle()
            }
            .font(.title2.bold())
            .disabled(privacy && terms ? false : true)
            .frame(width: 200, height: 50)
            .cornerRadius(10)
            .background(privacy && terms ? Color.greenMain : Color.darkBlue)
            .foregroundColor(.white)
            .padding(EdgeInsets(top: 5, leading: 0, bottom: 20, trailing: 15))
            .fullScreenCover(isPresented: $checked){
                AuthenticationView()
            }
        }
    }
    
}
