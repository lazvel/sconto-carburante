//
//  TermsAndCondtionView.swift
//  ScontoCarburante
//
//  Created by aleksandar.aleksic on 22.11.21..
//

import SwiftUI

struct TermsAndCondtionsView: View {
    @State private var privacy = false
    @State private var terms = false
    @State private var alertIsShown = false
    
    private let termsAndCond = DemoData.termsOfUse
    
    var body: some View {
        Text("Nesto")
        ZStack{
            VStack{
                Section{
                    VStack{
                        Image("roundedCheck")
                            .frame(maxWidth: .infinity)
                            .padding(EdgeInsets(top: 12, leading: 5, bottom: 10, trailing: 250))

                        Text(termsAndCond.title)
                            .font(.title2)
                            .fontWeight(.semibold)
                            .padding(EdgeInsets(top: 0, leading: 25, bottom: 0, trailing: 25))
                            .foregroundColor(.greenMain)

                    }
                    .frame(maxWidth: .infinity, minHeight: 180, alignment: .top)
                    .background(Color.firstToWhite)
                }
                .padding(EdgeInsets(top: 5, leading: 0, bottom: 10, trailing: 0))

                Section{
                    VStack(spacing: 10){
                        SectionTitleWithLink(title: termsAndCond.consent["privacy"]![0].title,
                                             url: termsAndCond.consent["privacy"]![0].url)
                        Checkbox(value: $privacy,
                                 textMessage: termsAndCond.consent["privacy"]![0].description)
                    }

                    VStack(spacing: 10){
                        SectionTitleWithLink(title: termsAndCond.consent["termsOfUse"]![0].title,
                                             url: termsAndCond.consent["termsOfUse"]![0].url)
                        Checkbox(value: $terms,
                                 textMessage: termsAndCond.consent["termsOfUse"]![0].description)
                    }
                }
                Spacer()

                ButtonTermsAndCondition(privacy: $privacy, terms: $terms)
            }
            if privacy && terms && !alertIsShown{
                if let alertText = DemoData.alerts[0],
                   let buttonTitle = DemoData.buttonTitles["close"]{
                    AlertView(title: alertText.title,
                              description: alertText.description,
                              buttonTitle: buttonTitle,
                              alertType: .alert,
                              alertIsShown: $alertIsShown)
                }
            }
        
        }
    }
    
    private func checkCheckboxes() -> Bool{
        if privacy, terms{
            return true
        }
        return false
    }
    
}

struct TermsAndCondtionView_Previews: PreviewProvider {
    static var previews: some View {
        TermsAndCondtionsView()
    }
}
