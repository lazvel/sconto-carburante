//
//  AuthenticationView.swift
//  ScontoCarburanteDemo
//
//  Created by lazar.velimirovic on 11/23/21.
//

import SwiftUI

struct AuthenticationView: View {
    @State private var isShowingSafe = false
    
    var body: some View {
        ZStack {
            VStack {
                TopBannerView()
                MenuView()
                Spacer(minLength: 30)
                BodyView(isShowingSafe: $isShowingSafe)
                Spacer(minLength: 40)
            }
        }
    }
}

struct TopBannerView: View {
    var body: some View {
        HStack {
            Image("Group")
                .renderingMode(.template)
                .foregroundColor(Color.white)
                .padding(20)
            Spacer()
        }
        .background(Color.greenMain)
    }
}
struct MenuView: View {
    var body: some View {
        HStack {
            Image("fill")
                .renderingMode(.template)
                .padding(20)
            Text("Servizio di autenticazione")
                .foregroundColor(Color.gray)
                .font(.headline)
            Spacer()
        }
        .background(Color.white)
        .shadow(color: Color.gray, radius: 10, x: 0, y: 2)
    }
}

struct BodyView: View {
    @Binding var isShowingSafe: Bool
    
    var body: some View {
        VStack {
            Group {
                Text("Accedi con la\ntua identita digitale")
                    .multilineTextAlignment(.center)
                    .foregroundColor(.darkBlue)
                    .font(.title.bold())
                    .padding(.bottom, 30)
                Button {
                    isShowingSafe.toggle()
                } label: {
                    HStack {
                        Image(systemName: "person.circle.fill")
                        Divider()
                            .frame(width: 2, height: 20)
                         .background(Color.white)
                        Text("Entra con SPID")
                            .font(.headline.bold())
                    }
                    .padding(15)
                }
                .foregroundColor(.white)
                .background(Color.buttonBlue)
                .cornerRadius(8)
                .fullScreenCover(isPresented: $isShowingSafe) {
                    SafeView()
                }
            }
            
            Group {
                Image("spid")
                    .padding(.vertical, 20)
                
                Divider()
                    .frame(width: UIScreen.main.bounds.width - 30 ,height: 1.5)
                 .background(Color.greenMain)
                
            }
            
            Group {
                Text("Oppure utiliza altri metodi di accesso")
                    .foregroundColor(.textGray)
                Spacer(minLength: 20)
  
                Image("OTP")
                    .resizable()
                    .frame(width: 60, height: 60, alignment: .center)
                
                Text("OTP di\nRegione Lombardia")
                    .font(.headline.bold())
                    .multilineTextAlignment(.center)
            }
        }
    }
}

struct AuthenticationView_Previews: PreviewProvider {
    static var previews: some View {
        AuthenticationView()
    }
}
