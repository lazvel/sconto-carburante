//
//  Vehicle.swift
//  ScontoCarburante
//
//  Created by aleksandar.aleksic on 24.11.21..
//

import Foundation

struct Vehicle{
    var id = UUID()
    var name: String
    var type: VehicleType
}

enum VehicleType{
    case car
    case scooter
}
