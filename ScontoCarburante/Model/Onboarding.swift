//
//  Onboarding.swift
//  ScontoCarburante
//
//  Created by aleksandar.aleksic on 23.11.21..
//

import Foundation

struct Onboarding{
    var onboardingProcess: Int
    var imageName: String
    var title: String
    var subtitle: String?
    var description: String
}

struct Consent{
    var title: String
    var consent: [String: [TermAndCondition]]
}

struct TermAndCondition{
    var title: String
    var description: String
    var url: String
}
