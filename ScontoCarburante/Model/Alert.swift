//
//  AlertType.swift
//  ScontoCarburante
//
//  Created by aleksandar.aleksic on 23.11.21..
//

import Foundation

enum AlertType{
    case alert, warrning, success
}

struct AlertText{
    var title: String
    var description: String
}
