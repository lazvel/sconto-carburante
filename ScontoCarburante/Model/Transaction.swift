//
//  Transaction.swift
//  ScontoCarburante
//
//  Created by aleksandar.aleksic on 29.11.21..
//

import Foundation

struct Transaction: Identifiable{
    let id = UUID()
    
    var date: String
    var amount: Double
    
    var saleCode: String
    var pricePerLiter: Double
    var dispensedLiters: Double
    var gross: Double
    
    var discountEurByLiter: Double
    var discountPerLiter: Double
    var discountedLiters: Double
    var discount: Double
    
    var amountToBePaid: Double
    var residual: String
    var refuelingDate: String
    var refuelingTime: String
}
