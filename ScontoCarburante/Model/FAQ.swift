//
//  FAQ.swift
//  ScontoCarburanteDemo
//
//  Created by lazar.velimirovic on 12/2/21.
//

import Foundation

struct FAQ: Identifiable {
    let id = UUID()
    
    let title: String
    let insideTitle: String
    let description: String
    
    static let faqs: [FAQ] = [
        FAQ(title: "Come avviene un nuovo rifornimento?", insideTitle: "Come aggiungere una nuova\ntarga tra i miei veicolli?", description: "Per aggiungere una nuova targa e necessario effettuare l'accesso al portale istituzionale e seguire le istruzioni"),
        FAQ(title: "Come aggiungere una nuova\ntarga tra i miei veicoli?", insideTitle: "Come aggiungere una nuova\ntarga tra i miei veicolli?", description: "Per aggiungere una nuova targa e necessario effettuare l'accesso al portale istituzionale e seguire le istruzioni"),
        FAQ(title: "Quanti rifornimenti e possibile fare in una settimana?", insideTitle: "Come aggiungere una nuova\ntarga tra i miei veicolli?", description: "Per aggiungere una nuova targa e necessario effettuare l'accesso al portale istituzionale e seguire le istruzioni")
    ]
}
