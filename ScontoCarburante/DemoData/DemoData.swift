//
//  DemoData.swift
//  ScontoCarburante
//
//  Created by aleksandar.aleksic on 19.11.21..
//

import Foundation
import SwiftUI

class DemoData{
    
    static let termsOfUse: Consent =
        Consent(title: "Leggi e accetta i termini di utillzzo e l'informativa privacy",
                consent: ["privacy": [TermAndCondition(title: "Leggi i'informativa privacy",
                                                       description: "Ho letto e compreso quanto indicato nell'informativa privacy",
                                                       url: "https://www.eng.it")],
                          "termsOfUse": [TermAndCondition(title: "Leggi i termini di utilizzo",
                                                          description: "Ho letto e compreso quanto indicato nei termini di utilizzo",
                                                          url: "https://www.eng.it")]
                         ])
    
    
    static let alerts: [Int: AlertText] = [
        0 : AlertText(title: "Attenzione",
                      description: "Per utilizzare l'app, e necessario accettare sia termini di utilizzo sia l'informativa sulla privacy."),
        1 : AlertText(title: "Non hai veicoli",
                      description: "Per poter utilizzare L'App devi aver registrato almeno un veicolo (auto o moto) presso...")
    ]
    
    
    static let buttonTitles: [String: String] = [
        "continue":"Continua",
        "cancel": "Salta",
        "goOn": "Prosegui",
        "close": "Chiudi",
        "authButton": "Entra con SPID",
        "reject": "Rifiuta",
        "accept": "Accetta",
        "sigIn": "Accedi"
    ]
    
    
    static let vehicles = [Vehicle(name: "CX870V", type: .car),
                           Vehicle(name: "SZ888V", type: .car),
                           Vehicle(name: "MK321I", type: .scooter),
                           Vehicle(name: "LL021I", type: .scooter)]
    
    static var notifications: [Notification] = [
        Notification(title:"Perezzo Diesel aumentato",
                     description: "Da oggi il prezzo del diesel subisce una variazione: incremento delllo 0.5%"),
        Notification(title: "Perezzo Diesel aumentato",
                     description: "Da oggi il prezzo del diesel subisce una variazione: incremento delllo 0.5%")]

    static var transactions: [Transaction] = [
        Transaction(date: "28/09/2020",
                    amount: 3.2,
                    saleCode: "45289435",
                    pricePerLiter: 1.769,
                    dispensedLiters: 4.22,
                    gross: 54.00,
                    discountEurByLiter: 0.190,
                    discountPerLiter: 1.701,
                    discountedLiters: 4.22,
                    discount: 5,
                    amountToBePaid: 49.00,
                    residual: "G=78 M=200",
                    refuelingDate: "28/09/2020",
                    refuelingTime: "15:30"),
        Transaction(date: "29/09/2020",
                    amount: 3.2,
                    saleCode: "45289436",
                    pricePerLiter: 1.769,
                    dispensedLiters: 4.22,
                    gross: 54.00,
                    discountEurByLiter: 0.190,
                    discountPerLiter: 1.701,
                    discountedLiters: 4.22,
                    discount: 5,
                    amountToBePaid: 49.00,
                    residual: "G=78 M=200",
                    refuelingDate: "29/09/2020",
                    refuelingTime: "11:30"),
        Transaction(date: "30/09/2020",
                    amount: 3.2,
                    saleCode: "45289437",
                    pricePerLiter: 1.769,
                    dispensedLiters: 4.22,
                    gross: 54.00,
                    discountEurByLiter: 0.190,
                    discountPerLiter: 1.701,
                    discountedLiters: 4.22,
                    discount: 5,
                    amountToBePaid: 49.00,
                    residual: "G=78 M=200",
                    refuelingDate: "30/09/2020",
                    refuelingTime: "11:30"),
        
    ]
}

