//
//  FuelHistory.swift
//  ScontoCarburanteDemo
//
//  Created by lazar.velimirovic on 12/1/21.
//

import Foundation

struct FuelHistory: Identifiable {
    let id = UUID()
    
    let date: String
    let youSaved: String
    
    let pumpStationCode: String
    let litrePrice: String
    let litresFueled: String
    let gross: String
    
    let discountPerLitre: String
    let discountLitrePrice: String
    let discountedLitres: String
    let discount: String
    
    let amountToBePaid: String
    let remainingCeiling: String
    let refuelingDate: String
    let refuelingTime: String
    
    
    static var transactions: [FuelHistory] = [
            FuelHistory(date: "28/09/2020",
                        youSaved: "3.20€",
                        pumpStationCode: "45289435",
                        litrePrice: "1,769€",
                        litresFueled: "4,22L",
                        gross: "54,00€",
                        discountPerLitre: "0,190",
                        discountLitrePrice: "1,701€",
                        discountedLitres: "4,22€",
                        discount: "5,00€",
                        amountToBePaid: "49,00€",
                        remainingCeiling: "G=78 M=200",
                        refuelingDate: "28/09/2020",
                        refuelingTime: "15:30"
                       ),
            FuelHistory(date: "24/09/2020",
                        youSaved: "6,30€",
                        pumpStationCode: "45289435",
                        litrePrice: "1,769€",
                        litresFueled: "4,22L",
                        gross: "54,00€",
                        discountPerLitre: "0,190",
                        discountLitrePrice: "1,701€",
                        discountedLitres: "4,22€",
                        discount: "5,00€",
                        amountToBePaid: "49,00€",
                        remainingCeiling: "G=78 M=200",
                        refuelingDate: "28/09/2020",
                        refuelingTime: "15:30"
                       ),
            FuelHistory(date: "14/09/2020",
                        youSaved: "8,50€",
                        pumpStationCode: "45289435",
                        litrePrice: "1,769€",
                        litresFueled: "4,22L",
                        gross: "54,00€",
                        discountPerLitre: "0,190",
                        discountLitrePrice: "1,701€",
                        discountedLitres: "4,22€",
                        discount: "5,00€",
                        amountToBePaid: "49,00€",
                        remainingCeiling: "G=78 M=200",
                        refuelingDate: "28/09/2020",
                        refuelingTime: "15:30"
                       ),
            FuelHistory(date: "10/09/2020",
                        youSaved: "5,00€",
                        pumpStationCode: "45289435",
                        litrePrice: "1,769€",
                        litresFueled: "4,22L",
                        gross: "54,00€",
                        discountPerLitre: "0,190",
                        discountLitrePrice: "1,701€",
                        discountedLitres: "4,22€",
                        discount: "5,00€",
                        amountToBePaid: "49,00€",
                        remainingCeiling: "G=78 M=200",
                        refuelingDate: "10/09/2020",
                        refuelingTime: "15:30"
                       ),
            FuelHistory(date: "08/09/2020",
                        youSaved: "4,50€",
                        pumpStationCode: "45289435",
                        litrePrice: "1,769€",
                        litresFueled: "4,22L",
                        gross: "54,00€",
                        discountPerLitre: "0,190",
                        discountLitrePrice: "1,701€",
                        discountedLitres: "4,22€",
                        discount: "5,00€",
                        amountToBePaid: "49,00€",
                        remainingCeiling: "G=78 M=200",
                        refuelingDate: "08/09/2020",
                        refuelingTime: "15:30"
                       )
        ]
}
